import React, { Component } from 'react';
import Header from './components/Header';
import Header2 from './components/Header2';
import Wallet from './components/Wallet';
import Wallet2 from './components/Wallet2';


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
        <Header />
        <Wallet /> 
        <Header2 />
        <Wallet2 />
        
        </header>
      </div>
    );
  }
}

export default App;
